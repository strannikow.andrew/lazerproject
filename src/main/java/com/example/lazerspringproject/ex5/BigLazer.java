package com.example.lazerspringproject.ex5;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class BigLazer implements Gun{
    @Override
    public int shoot() {
        log.info("PABAM!!");
        return 100;
    }
}
