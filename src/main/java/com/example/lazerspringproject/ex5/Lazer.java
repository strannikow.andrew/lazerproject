package com.example.lazerspringproject.ex5;

import org.springframework.beans.factory.annotation.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Lazer implements Gun{

    @Value("${lazer.sound.text:pif paf}")
    private String shootingSound;
    public int shoot(){
        log.info(shootingSound);
        return 40;

    }
}
