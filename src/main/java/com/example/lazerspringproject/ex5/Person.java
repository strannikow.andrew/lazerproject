package com.example.lazerspringproject.ex5;


import org.springframework.stereotype.Component;

@Component

public class Person {
    private  Gun lazer;
    public int shoot(){
       return lazer.shoot();
    }

    public void setLazer(Gun lazer) {
        this.lazer = lazer;
    }
}
