package com.example.lazerspringproject.ex5;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


@Component
@Slf4j

public class Person2  {
    private Gun weapon;

    public Person2(@Qualifier("bigLazer") Gun weapon) {
        this.weapon = weapon;
    }

    public int shoot(){
        return weapon.shoot();
    }

    public void setWeapon(Gun weapon) {
        this.weapon = weapon;
    }
}
