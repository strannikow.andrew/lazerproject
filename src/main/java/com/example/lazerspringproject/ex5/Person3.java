package com.example.lazerspringproject.ex5;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


@Component
@Slf4j

public class Person3  {
    private Gun smallLazer;

    public Person3(@Qualifier("smallLazer") Gun smallLazer) {
        this.smallLazer = smallLazer;
    }

    public int shoot(){
         return smallLazer.shoot();
    }

    public void setSmallLazer(Gun smallLazer) {
        this.smallLazer = smallLazer;
    }
}