package com.example.lazerspringproject.ex5;

import com.example.lazerspringproject.ex6.CustomConfigurationComponent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShootingRange implements CommandLineRunner {

    private final Person person;
    private final Person2 person2;
    private final  Person3 person3;
    private final CustomConfigurationComponent customConfigurationComponent;

    private final List<Gun> weapons;

    @Override
    public void run(String... args) throws Exception {

        log.info(customConfigurationComponent.toString());
        Random r = new Random();
        int low = 0;
        int high = 2;
        int result = r.nextInt(high-low)+ low;
        log.info(result+"");

        person.setLazer(weapons.get(result));
         result = r.nextInt(high-low)+ low;
        log.info(result+"");
        person2.setWeapon(weapons.get(result));
        result = r.nextInt(high-low)+ low;
        log.info(result+"");
        person3.setSmallLazer(weapons.get(result));

        if(person.shoot()>= person2.shoot()){
            log.info(person.getClass() + "wins. ");
        }else{
            log.info(person2.getClass() + "wins. ");
        }

//        person.shoot();
//        person2.shoot();
//        person3.shoot();
    }
}
