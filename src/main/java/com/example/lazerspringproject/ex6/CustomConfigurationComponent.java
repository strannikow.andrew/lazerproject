package com.example.lazerspringproject.ex6;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import lombok.ToString;

import org.hibernate.validator.constraints.Length;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;

@ConfigurationProperties(prefix = "pl.sdacademy.zad6")
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Validated
public class CustomConfigurationComponent {
    @NotNull
    @Email
    private String email;
    private String firstName;
    @Length(min =3 , max = 20)
    private String lastName;

    private String address;
    @Min(18)
    private int age;
    @NotEmpty
    private List<String> values;
    @NotEmpty
    private Map<String,String> customAttributes;

    @AssertTrue
    private boolean isAdressValid(){
        return address !=null && address.split(" ").length==2;

    }

}
