package com.example.lazerspringproject.ex9;

import lombok.RequiredArgsConstructor;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import static com.example.lazerspringproject.ex9.FileDataRestController.FILE_DATA_CONTROLER_PATH;

@RestController
@RequiredArgsConstructor
@RequestMapping(FILE_DATA_CONTROLER_PATH)
public class FileDataRestController {

    public static final String FILE_DATA_CONTROLER_PATH = "/api/files-data";

    private final FileDataService fileDataService;
    //GET /api/files-data - returns all FileData objects from the database as a JSON object (not a list)
    @GetMapping
    public FileDataWrapper getAll(){
        return new FileDataWrapper(fileDataService.getAll());
    }
    //GET /api/files-data/{id} - returns a FileData object with
    // a specific identifier (or throws an exception SdaException)
    @GetMapping("/{id}")
    public FileData getById(@PathVariable("id") UUID id ){
        return fileDataService.getById(id);

    }
    //POST /api/files-data - creates a FileData object and writes it to the database.
    // Returns the status 201 and in the Location header, the URI to get it.
    @PostMapping
    public ResponseEntity save(@RequestBody FileData fileData){
        FileData savedFileData = fileDataService.save(fileData);
        return ResponseEntity
                .created(URI.create("localhost:8080"+FILE_DATA_CONTROLER_PATH + "/" + fileData.getId()))
                .body(savedFileData);
    }
    //PUT /api/files-data/{id} - updates an existing FileData object stored in the database.
    // Returns status * 204 * (or throws SdaException when no object with the given id exists).

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable("id") UUID id,
                                 @RequestBody FileData fileData){
        fileDataService.update(id,fileData);
        return ResponseEntity.status(204).build();
//  DELETE /api/files-data/{id} - removes an existing FileData object stored in the database.
//  Returns status * 204 * (or throws SdaException when no object with the given id exists).
    }
    @DeleteMapping("/{id}")
    public ResponseEntity delete(
            @PathVariable("id") UUID id) {
        fileDataService.delete(id);
        return ResponseEntity.status(204).build();
    }


}
