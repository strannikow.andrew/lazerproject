package com.example.lazerspringproject.ex9;

public class SdaException extends RuntimeException {
    public SdaException(final String message) {
        super(message);
    }
}