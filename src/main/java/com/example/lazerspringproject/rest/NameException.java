package com.example.lazerspringproject.rest;

public class NameException extends Throwable {
    public NameException(String message) {
        super(message);
    }
}
