package com.example.lazerspringproject.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Profile("dev")
@RestController
@RequiredArgsConstructor
public class TestApi {

    private final PersonRepository repository;


    @GetMapping("/{name}")
    public String sayhello(@PathVariable String firstName) {
        return "Hello" + firstName;
    }

    @GetMapping
    public List<Person> getAll() {
        return repository.findAll();
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Person saveName(@RequestBody Person person) {
        repository.save(person);
        return person;
    }

    @PutMapping("/{name}")
    public String updateAllPersomsWithName(@PathVariable("name") String firstName,
                                            @Valid @RequestBody Person person) {
        List<Person> allPersonWithName = repository.findByName(firstName);
        List<Person> newPersons = allPersonWithName.stream().
                map(personFromList -> {
            personFromList.setAge(person.getAge());
            personFromList.setName(person.getName());
            return personFromList;
        }).collect(Collectors.toList());
        repository.saveAll(newPersons);

        return "Updated";
    }


    @DeleteMapping("/{name}")
        public String delete(@PathVariable("name") String firstName) throws NameException {
            if(firstName.matches(".*\\d.*")){
                throw new NameException("Name must not contain numbers.");
            }
            List<Person> persons = repository.findByName(firstName);
            if (persons != null && persons.size() > 0) {
                repository.deleteAll(persons);
                return "Deleted";
            }
            return "Not deleted";
        }
    }

