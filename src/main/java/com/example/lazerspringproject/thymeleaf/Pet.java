package com.example.lazerspringproject.thymeleaf;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.validation.constraints.NotBlank;


@Entity
@Data
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;
    @NotBlank(message = " Pet name can't be empty")
    private String name;
    private int age;
    @NotBlank(message = " Pet type can't be empty(CAT,MOUSE,HORSE)")
    private String type;
}
