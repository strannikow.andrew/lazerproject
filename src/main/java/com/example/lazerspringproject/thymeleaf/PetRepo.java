package com.example.lazerspringproject.thymeleaf;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepo extends JpaRepository<Pet,Long> {
}
